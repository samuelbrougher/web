
let button = document.getElementById("button1");


//Following code is for the frequency domain analysis of the microphone input (steth audio)--this shows the decibel level of individual frequencies 
button.addEventListener("click", function() {

  // monkeypatch Web Audio
  window.AudioContext = window.AudioContext || window.webkitAudioContext;

  // grab an audio context
  audioContext = new AudioContext();

  // Attempt to get audio input
  try {
      // monkeypatch getUserMedia
      navigator.getUserMedia = 
        navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia;

      // ask for an audio input
      navigator.getUserMedia(
      {
          "audio": {
              "mandatory": {
                  "googEchoCancellation": "false",
                  "googAutoGainControl": "false",
                  "googNoiseSuppression": "false",
                  "googHighpassFilter": "false"
              },
              "optional": []
          },
      }, gotStream1, didntGetStream);
  } catch (e) {
      alert('getUserMedia threw exception :' + e);
  }
});

function didntGetStream() {
  alert('Stream generation failed.');
}

var mediaStreamSource = null;

function gotStream1(stream) {
  // Create an AudioNode from the stream.
  mediaStreamSource = audioContext.createMediaStreamSource(stream);

  let analyser = audioContext.createAnalyser();
  analyser.fftSize = 2048;
  var bufferLength = analyser.frequencyBinCount;
  let lowpassFilter = audioContext.createBiquadFilter();

//this is where basic filtering is occuring--simply filtering out audio outside of the 20-1500 hz band
let highpassFilter = audioContext.createBiquadFilter();
    highpassFilter.type = "highpass";
    highpassFilter.frequency.value = 20;
    lowpassFilter.type = "lowpass";
    lowpassFilter.frequency.value = 1500;

    //now we have to connect the steth audio stream to the lowpass filter, then highpass, then finally to the analyser 
  mediaStreamSource.connect(lowpassFilter);
  lowpassFilter.connect(highpassFilter);
  highpassFilter.connect(analyser);


  const dataArray = new Uint8Array(bufferLength);
// fill the Float32Array with data returned from getFloatFrequencyData()
  analyser.getByteFrequencyData(dataArray);

  //beginning of animation code--creating a canvas where we'll draw the visualization 
  const canvas = document.createElement('canvas');
    canvas.style.position = 'relative';
    canvas.style.top = 0;
    canvas.style.left = 0;
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight/2;
    document.body.appendChild(canvas);
    const canvasCtx = canvas.getContext('2d');
    canvasCtx.clearRect(0, 0, canvas.width, canvas.height);

    //animation method that is called recursively by "requestAnimationFrame(draw)" -- gets audio data on each call and animates it 
    function draw() {
      drawVisual = requestAnimationFrame(draw);
    
      analyser.getByteFrequencyData(dataArray);
    
      canvasCtx.fillStyle = 'rgb(0, 0, 0)';
      canvasCtx.fillRect(0, 0, canvas.width, canvas.height);
    
      var barWidth = (canvas.width / bufferLength) * 10;
      var barHeight;
      var x = 0;
    
      for(var i = 0; i < bufferLength; i++) {
        barHeight = dataArray[i];
    
        canvasCtx.fillStyle = 'rgb(' + (barHeight+100) + ',50,50)';
        canvasCtx.fillRect(x,canvas.height-barHeight/2,barWidth,barHeight/2);
    
        x += barWidth + 1;
      }
    };
    draw();
}



let button2 = document.getElementById("button2");

//Following code is for the time domain analysis of the microphone input (steth audio)--this shows the wave form of the steth signal

button2.addEventListener("click", function() {

  // monkeypatch Web Audio
  window.AudioContext = window.AudioContext || window.webkitAudioContext;

  // grab an audio context
  audioContext = new AudioContext();

  // Attempt to get audio input
  try {
      // monkeypatch getUserMedia
      navigator.getUserMedia = 
        navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia;

      // ask for an audio input
      navigator.getUserMedia(
      {
          "audio": {
              "mandatory": {
                  "googEchoCancellation": "false",
                  "googAutoGainControl": "false",
                  "googNoiseSuppression": "false",
                  "googHighpassFilter": "false"
              },
              "optional": []
          },
      }, gotStream, didntGetStream);
  } catch (e) {
      alert('getUserMedia threw exception :' + e);
  }
});

function didntGetStream() {
  alert('Stream generation failed.');
}

var mediaStreamSource = null;

function gotStream(stream) {
  // Create an AudioNode from the stream.
    mediaStreamSource = audioContext.createMediaStreamSource(stream);

    let analyser = audioContext.createAnalyser();
    analyser.smoothingTimeConstant = 0.99;

    //this is where basic filtering is occuring--simply filtering out audio outside of the 50-1000 hz band

    let lowbiquadFilter = audioContext.createBiquadFilter();
    lowbiquadFilter.type = "lowpass";
    let highbiquadFilter = audioContext.createBiquadFilter();
    highbiquadFilter.type = "highpass";
    lowbiquadFilter.frequency.value = 1000;
    highbiquadFilter.frequency.value = 50;

    //now we have to connect the steth audio stream to the lowpass filter, then highpass, then finally to the analyser 
    mediaStreamSource.connect(lowbiquadFilter);
    lowbiquadFilter.connect(highbiquadFilter);
    highbiquadFilter.connect(analyser);

    analyser.fftSize = 4096*2*2*2;

    const bufferLength = analyser.fftSize;
    const myDataArray = new Float32Array(4096*2*2*2);

  //fill the Float32Array with data returned from getFloatFrequencyData()
    analyser.getFloatTimeDomainData(myDataArray);

  //beginning of animation code--creating a canvas where we'll draw the visualization 

    const canvas = document.createElement('canvas');
    canvas.style.position = 'relative';
    canvas.style.top = 0;
    canvas.style.left = 0;
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight/2;
    document.body.appendChild(canvas);
    const canvasCtx = canvas.getContext('2d');
    canvasCtx.clearRect(0, 0, canvas.width, canvas.height);


    //animation method that is called recursively by "requestAnimationFrame(draw)" -- gets audio data on each call and animates it 
    function draw() {
        analyser.getFloatTimeDomainData(myDataArray);
        drawVisual = requestAnimationFrame(draw);
        canvasCtx.fillStyle = 'rgb(200, 200, 200)';
        canvasCtx.fillRect(0, 0, canvas.width, canvas.height);
        canvasCtx.lineWidth = 5;
        canvasCtx.strokeStyle = 'rgb(0, 0, 0)';

        const sliceWidth = (canvas.width * 1.0 / bufferLength);
        let x = 0;

        canvasCtx.beginPath();
        for(var i = 0; i < bufferLength; i++) {
            const v = myDataArray[i]*(canvas.height);
            const y = v +  canvas.height/2;
            if(i === 0) {
                canvasCtx.moveTo(x, y);} 
            else {   
                canvasCtx.lineTo(x, y);
            }
            x += sliceWidth;
        }
        canvasCtx.lineTo(canvas.width, canvas.height/2);
        canvasCtx.stroke();
            };
    draw();
}

